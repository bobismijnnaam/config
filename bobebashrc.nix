{ pkgs, ... }:
{
  home.file.".bobebashrc".text = ''
    if [ "$(hostname)" == "kirk" ]; then
      # Has a \ before the $ strangely enough
      # And there are control codes after the $ to clear color that need
      # to be copied as well.
      export PS1="$(echo $PS1 | sed -E 's/\\(\$.*)$/\n\1 /')"
    fi

    if command -v vim &> /dev/null; then
      export EDITOR=vim
    fi

    # Start tmux if it's available
    if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
      exec tmux
    fi

    # PhD settings
    export PHDPATH=/home/bobe/PhD
    source $PHDPATH/admin/terminal_bindings.sh
  '';
}
