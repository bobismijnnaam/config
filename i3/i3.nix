let
  modifier = "Mod4";
in
{
  enable = true;
  config = let
    mod = "Mod4";
    refresh_i3status = "killall -SIGUSR1 i3status";
    ws1 = "1: web";
    ws2 = "2: code";
    ws3 = "3: code";
    ws4 = "4: skype";
    ws5 = "5: mail";
    ws6 = "6: spotify";
    ws7 = "7";
    ws8 = "8";
    ws9 = "9";
    ws10 = "10";
    Locker = "i3lock -c 222222";
    sys_menu_mode = "System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown";
  in {
    keybindings = {
      "XF86AudioRaiseVolume" = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && ${refresh_i3status}";
      "XF86AudioLowerVolume" = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && ${refresh_i3status}";
      "XF86AudioMute" = "exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && ${refresh_i3status}";
      "XF86AudioMicMute" = "exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && ${refresh_i3status}";
      "${mod}+Return" = "exec i3-sensible-terminal";
      "${mod}+Shift+q" = "kill";
      "${mod}+d" = "exec dmenu_run";
      "${mod}+h" = "focus left";
      "${mod}+j" = "focus down";
      "${mod}+k" = "focus up";
      "${mod}+l" = "focus right";
      "${mod}+Left" = "focus left";
      "${mod}+Down" = "focus down";
      "${mod}+Up" = "focus up";
      "${mod}+Right" = "focus right";
      "${mod}+Shift+h" = "move left";
      "${mod}+Shift+j" = "move down";
      "${mod}+Shift+k" = "move up";
      "${mod}+Shift+l" = "move right";
      "${mod}+Shift+Left" = "move left";
      "${mod}+Shift+Down" = "move down";
      "${mod}+Shift+Up" = "move up";
      "${mod}+Shift+Right" = "move right";
      "${mod}+Shift+backslash" = "split h";
      "${mod}+minus" = "split v";
      "${mod}+f" = "fullscreen toggle";
      "${mod}+s" = "layout stacking";
      "${mod}+w" = "layout tabbed";
      "${mod}+e" = "layout toggle split";
      "${mod}+Shift+space" = "floating toggle";
      "${mod}+space" = "focus mode_toggle";
      "${mod}+a" = "focus parent";
      "${mod}+Shift+a" = "focus child";
      "${mod}+1" = "workspace number ${ws1}";
      "${mod}+2" = "workspace number ${ws2}";
      "${mod}+3" = "workspace number ${ws3}";
      "${mod}+4" = "workspace number ${ws4}";
      "${mod}+5" = "workspace number ${ws5}";
      "${mod}+6" = "workspace number ${ws6}";
      "${mod}+7" = "workspace number ${ws7}";
      "${mod}+8" = "workspace number ${ws8}";
      "${mod}+9" = "workspace number ${ws9}";
      "${mod}+0" = "workspace number ${ws10}";
      "${mod}+Shift+1" = "move container to workspace number ${ws1}";
      "${mod}+Shift+2" = "move container to workspace number ${ws2}";
      "${mod}+Shift+3" = "move container to workspace number ${ws3}";
      "${mod}+Shift+4" = "move container to workspace number ${ws4}";
      "${mod}+Shift+5" = "move container to workspace number ${ws5}";
      "${mod}+Shift+6" = "move container to workspace number ${ws6}";
      "${mod}+Shift+7" = "move container to workspace number ${ws7}";
      "${mod}+Shift+8" = "move container to workspace number ${ws8}";
      "${mod}+Shift+9" = "move container to workspace number ${ws9}";
      "${mod}+Shift+0" = "move container to workspace number ${ws10}";
      "${mod}+Shift+c" = "reload";
      "${mod}+Shift+r" = "restart";
      "${mod}+Shift+e" = "exec i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'";
      "${mod}+r" = "mode \"resize\"";
      "XF86AudioPlay" = "exec playerctl play-pause";
      "XF86AudioNext" = "exec playerctl next";
      "XF86AudioPrev" = "exec playerctl previous";
      "${mod}+Shift+p" = "exec playerctl play-pause";
      "Ctrl+${mod}+Right" = "exec playerctl next";
      "Ctrl+${mod}+Left" = "exec playerctl previous";
      "Print" = "exec scrot '%F_%T.png' -e 'xclip -selection clipboard -t image/png -i $f && rm $f'";
      "Ctrl+Print" = "exec scrot '%F_%T.png' --focused -e 'xclip -selection clipboard -t image/png -i $f && rm $f'";
      "${mod}+Pause" = "mode \"${sys_menu_mode}\"";
      "${mod}+c" = "move position center";
      "XF86MonBrightnessUp" = "exec --no-startup-id brightnessctl s +10%";
      "XF86MonBrightnessDown" = "exec --no-startup-id brightnessctl s 10%-";
      "${mod}+p" = "exec --no-startup-id autorandr --change --skip-option crtc";
    };

    keycodebindings = {
      "${mod}+49" = "exec xfce4-terminal --drop-down";
    };

    fonts = [ "pango:DejaVu Sans Mono 8" ];

    startup = [
      {
        command = "xss-lock --transfer-sleep-lock -- ${Locker} --no-fork";
        always = false;
        notification = false;
      }
      {
        command = "autorandr --change";
        always = false;
        notification = false;
      }
      {
        command = "xsetroot -solid '#222222'";
        always = false;
        notification = false;
      }
      {
        command = "dropbox start";
        always = false;
        notification = false;
      }
      {
        command = "skypeforlinux";
        always = false;
        notification = false;
      }
      {
        command = "dunst";
        always = false;
        notification = false;
      }
      {
        command = "nm-applet";
        always = false;
        notification = false;
      }
      {
        command = "~/UNSAFE/Tools/map-politie/map-politie.py --tray";
        always = false;
        notification = false;
      }
      {
        # With accents:
        # setxkbmap us -variant alt-intl
        command = "setxkbmap us -variant euro";
        always = false;
        notification = false;
      }
    ];

    modes = {
      resize = {
          "h" = "resize shrink width 10 px or 10 ppt";
          "j" = "resize grow height 10 px or 10 ppt";
          "k" = "resize shrink height 10 px or 10 ppt";
          "l" = "resize grow width 10 px or 10 ppt";
          "Left" = "resize shrink width 10 px or 10 ppt";
          "Down" = "resize grow height 10 px or 10 ppt";
          "Up" = "resize shrink height 10 px or 10 ppt";
          "Right" = "resize grow width 10 px or 10 ppt";
          "Return" = "mode \"default\"";
          "Escape" = "mode \"default\"";
          "${mod}+r" = "mode \"default\"";
      };
      "${sys_menu_mode}" = {
          "l" = "exec --no-startup-id ${Locker}, mode \"default\"";
          "e" = "exec --no-startup-id i3-msg exit, mode \"default\"";
          "s" = "exec --no-startup-id ${Locker} && systemctl suspend, mode \"default\"";
          "h" = "exec --no-startup-id ${Locker} && systemctl hibernate, mode \"default\"";
          "r" = "exec --no-startup-id systemctl reboot, mode \"default\"";
          "Shift+s" = "exec --no-startup-id systemctl poweroff -i, mode \"default\"";
          "Return" = "mode \"default\"";
          "Escape" = "mode \"default\"";
      };
    };

    floating.modifier = "${mod}";

    assigns = {
      "${ws1}" = [{ class = "Firefox"; }];
      "${ws2}" = [{ class = "jetbrains-idea"; }];
      "${ws3}" = [{ class = "Zotero"; }];
      "${ws4}" = [{ class = "Skype"; }];
      "${ws5}" = [{ class = "Daily"; }]; # Apparently Thunderbird class is Daily atm
    };

    window.commands = [
      {
        command = "move to workspace ${ws6}";
        criteria = {
          class = "Spotify"; 
        };
      }
    ];

    bars = [
      {
        position = "top";
        statusCommand = "i3status";
      }
    ];
  };

  extraConfig = ''
    workspace "1: web" output DP-4 eDP-1-1
  '';
}
