{ pkgs, ... }:
let
  my-python-packages = python-packages: with python-packages; [
  ]; 
  python-with-my-packages = pkgs.python37.withPackages my-python-packages;
in with pkgs; 
[
  # Basic stuff

  python-with-my-packages

  # strace -e file man
  # https://github.com/NixOS/nixpkgs/issues/8398 
  # Also add stuff to bashrc:
  # export LOCALE_ARCHIVE="$(readlink ~/.nix-profile/lib/locale)/locale-archive"
  # pkgs.glibcLocales

  # For setting brightness on ubuntu
  # Make sure user is in video group: usermod -a -G video bobe

  pkgs.pubs
  pkgs.todo-txt-cli
]

