{ pkgs, ... }:
{
  imports = [
    ./vim.nix
  ];

  home.packages = with pkgs; [
    fortune
    xclip
    graphviz
    jdk11
    (sbt.override { jre = pkgs.jdk11; })
    tree
    htop
  ];
}
