{ config, pkgs, ... }:
{
  imports = [ 
    ./hardware-configuration.nix
  ];

  services.xserver.videoDrivers = [ 
    "nvidia" 
    # "modesetting" 
  ];

  hardware = {
    nvidia = {
      optimus_prime = {
        enable = true;
        nvidiaBusId = "PCI:1:0:0";
        intelBusId = "PCI:0:2:0";
      };
      # modesetting.enable = true;
    };
    
    opengl = {
      driSupport = true;
      driSupport32Bit = true;
    };
  };

  nixpkgs.config.allowUnfree = true;

  boot.loader.systemd-boot.enable = false;
  boot.loader.efi.canTouchEfiVariables = false;
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "lapbobe-nixos";
  networking.wireless = {
    enable = true;
    userControlled = {
      enable = true;
      group = "network";
    };

    networks = {
      Minos = {
        psk = "Welkom88!";
      };

      ZyXEL5ECECF = {
        psk = "79CCBAAF56D7";
      };

      Ziggo1315083 = {
        psk = "Frankrijklaan333";
      };
    };
  };

  networking.useDHCP = false;
  # networking.interfaces.enp61s0u1u3.useDHCP = true;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  time.timeZone = "Europe/Amsterdam";

  environment.systemPackages = with pkgs; [
    wget vim 
  ];

  programs.vim.defaultEditor = true;

  # TODO: Firewall?

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";
  services.xserver.libinput.enable = true;
  services.xserver.dpi = 96;

  users = {
    groups.network = { };
    users.bobe = {
      isNormalUser = true;
      extraGroups = [ "wheel" "network" ];
    };
  };

  system.stateVersion = "20.03";

  services.xserver = {
    enable = true;
    
    displayManager = {
      session = [
        {
          name = "home-manager";
          start = ''
            ${pkgs.runtimeShell} $HOME/.hm-xsession &
            waitPID=$!
          '';
          manage = "window";
        }
      ];
    };
  };
}

