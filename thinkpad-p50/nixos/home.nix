{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    firefox
    git
    tree
    i3status
    dmenu
    arandr

    # Temp stuff because I don't have my structure in order
    skype
    texstudio
    texlive.combined.scheme-full
    spotify

    cowsay
    fortune

    i3lock # TODO: Refactor this into i3 conf somehow
    xclip # TODO: Refactor this into tmux conf somehow
  ] ++ pkgs.callPackage ../../basicPackages.nix {};

  imports = [
    ../../i3/dunst.nix
    ../../vim.nix
    ../../tmux.nix
  ];

  home.stateVersion = "20.03";

  programs.home-manager.enable = true;

  xsession.enable = true;
  xsession.numlock.enable = true;
  xsession.scriptPath = ".hm-xsession";

  programs.kakoune = import ../../kakoune.nix;
  xdg.configFile."i3status/config".source = ../../i3/i3status.conf;

  xsession.windowManager.i3 = import ../../i3/i3.nix;

  # Investigate: xterm*loginshell: true?
  # Helpful: xterm -report-xres
  xresources.properties = {
    "xterm*faceName" = "DejaVu Sans Mono Book";
    "xterm*faceSize" = 11;
    "xterm*savelines" = 16384;
    ## Not used because of tmux:
    # "xterm*rightScrollBar" = true;
    # "xterm*ScrollBar" = true;
    # "xterm*scrollTtyOutput" = false;

    "xterm*VT100.Translations" = ''
      #override \
      Shift Ctrl<Key>V: insert-selection(CLIPBOARD) \n\
      Shift Ctrl<Key>V: insert-selection(PRIMARY) \n\
      Ctrl Shift <Key>C:    copy-selection(CLIPBOARD)
    '';
  };

  xresources.extraConfig = builtins.readFile (
    pkgs.fetchFromGitHub {
      owner = "solarized";
      repo = "xresources";
      rev = "025ceddbddf55f2eb4ab40b05889148aab9699fc";
      sha256 = "0lxv37gmh38y9d3l8nbnsm1mskcv10g3i83j0kac0a2qmypv1k9f";
    } + "/Xresources.light"
  );

  home.file.".bashrc".text = ''
    if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
      exec tmux
    fi

    fortune | cowsay -f bunny

    PS1="''${PS1//$/\n$}"
  '';

  # This is needed because otherwise tmux won't load bashrc
  home.file.".bash_profile".text = ''
    . ~/.bashrc
  '';
}
