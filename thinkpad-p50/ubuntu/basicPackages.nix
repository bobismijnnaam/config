{ pkgs, ... }:
let
  my-python-packages = python-packages: with python-packages; [
    pypandoc
    # pystray # Oops, not yet on 20.03

    pip
    setuptools
  ]; 
  python-with-my-packages = pkgs.python37.withPackages my-python-packages;
in with pkgs; 
[
  # Basic stuff

  pkgs.scrot # Screenshots
  pkgs.playerctl # Playing/pausing spotify

  python-with-my-packages

  # strace -e file man
  # https://github.com/NixOS/nixpkgs/issues/8398 
  # Also add stuff to bashrc:
  # export LOCALE_ARCHIVE="$(readlink ~/.nix-profile/lib/locale)/locale-archive"
  pkgs.glibcLocales

  pkgs.autorandr
  
  pkgs.thunderbird

  pkgs.vlc

  # For setting brightness on ubuntu
  # Make sure user is in video group: usermod -a -G video bobe
  pkgs.brightnessctl
  pkgs.xss-lock

  # Random GLX error, didn't work on ubuntu
  #anki

  pkgs.z-lua

  pkgs.pubs
]

