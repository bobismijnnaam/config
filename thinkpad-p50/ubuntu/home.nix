{ config, pkgs, ... }:
{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  imports = [
    ../../commandLine.nix
    ../../tmux.nix
  ];

  home.packages = pkgs.callPackage ../../basicPackages.nix {};

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "bobe";
  home.homeDirectory = "/home/bobe";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "20.09";

  programs.kakoune = import ../../kakoune.nix;
  xdg.configFile."i3status/config".source = ../../i3/i3status.conf;
  xsession.windowManager.i3 = import ../../i3/i3.nix;
}
