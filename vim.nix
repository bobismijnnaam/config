{ pkgs, ... }:
{
  programs.vim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      The_NERD_Commenter
      gitgutter
      vim-tmux-navigator
      vim-colors-solarized
    ];
    extraConfig = ''
      set nocompatible              " be iMproved, required
      filetype off                  " required

      " set the runtime path to include Vundle and initialize
      " set rtp+=~/.vim/bundle/Vundle.vim
      " call vundle#begin()

      " let Vundle manage Vundle, required
      " Plugin 'VundleVim/Vundle.vim'

      " NERDCommenter
      " Plugin 'scrooloose/nerdcommenter'

      " code::stats
      " Plugin 'https://gitlab.com/code-stats/code-stats-vim.git'

      " Plugin 'airblade/vim-gitgutter'

      " Plugin 'rust-lang/rust.vim'

      " Plugin 'herringtondarkholme/yats.vim'

      " Plugin 'christoomey/vim-tmux-navigator'

      " Plugin 'junegunn/fzf'
      " Plugin 'junegunn/fzf.vim'

      " Colorscheme stuff
      " Plugin 'altercation/vim-colors-solarized'
      " Plugin 'nightsense/night-and-day'

      " All of your Plugins must be added before the following line
      " call vundle#end()            " required
      filetype plugin indent on    " required

      "" Vundle end

      au BufNewFile,BufRead *.ldg,*.ledger setf ledger | comp ledger
      au BufRead,BufNewFile *.smv		setfiletype nusmv
      au BufRead,BufNewFile *.lrv		setfiletype larva
      au BufRead,BufNewFile *.mcrl2		setfiletype mcrl2
      au BufRead,BufNewFile *.mcrl		setfiletype mcrl
      au BufRead,BufNewFile *.ebnf		setfiletype ebnf
      au BufRead,BufNewFile *.bnf		setfiletype ebnf

      let mapleader = "\<Space>"

      " Colorscheme for vim?
      " set t_Co=256
      " let g:solarized_termcolors=256
      " Theme :o
      syntax enable
      " set background=light
      colorscheme ron 

      let g:nd_themes = [
        \ ['8:00',  'solarized', 'light' ],
        \ ['20:00', 'solarized',            'dark' ]
        \ ]

      set number

      " Normal backspace?
      set backspace=indent,eol,start

      " Just like the ubuntu terminal
      set guifont=Ubuntu\ Mono\ 13

      " Disable gui thingies
      set go-=m " remove menu bar
      set go-=T " remove toolbar
      set go-=r " remove right hand scroll bar
      set go-=L " remove left hand scroll bar   

      set hlsearch
      set incsearch

      " Searches for a makefile or does an upwards search for a directory named 'build'
      function Make()
              let currDir = './'
              while 1
                      let makeList = split(globpath(currDir, 'makefile')) + split(globpath(currDir, 'Makefile'))
                      let buildList = split(globpath(currDir, 'build'))
                      if len(makeList)
                              execute 'cd' fnameescape(currDir)
                              make | botright copen
                              cd -
                              break
                      elseif len(buildList)
                              let currDir = currDir . 'build/'
                      else
                              let currDir = currDir . '../'
                      endif
              endwhile
      endfunction

      nnoremap <F7> :silent make\|redraw!\|cw<CR>
      nmap <F8> :! make clean <CR> <F9>
      nmap <F9> :call Make()<CR>
      nmap <F10> :cclose <CR>
      nmap <Leader><End> :cclose <CR>

      " if cursor is on ([{, replaces it with the key pressed and it's neighbour as
      " well
      nmap <Leader>[ %r]``r[
      nmap <Leader>] %r[``r]
      nmap <Leader>{ %r}``r{
      nmap <Leader>} %r}``r{
      nmap <Leader>( %r)``r(
      nmap <Leader>) %r(``r)

      nmap <Leader><Return> :noh<Return>

      " formats selected text 80 columns wide
      vmap <Leader>8 :s/\n\n/\r\r\r/g<CR>gv:s/\([^\n]\+\)\n/\1 /g<CR>gv:s/\s\+/ /g<CR>gv:s/\s\+$//g<CR>gv:!fold --spaces --width=80 \| sed 's/[ \t]*$//'<CR>:noh<CR>

      " Latex word count
      map <Leader>lw :w !detex \| wc -w<CR>

      " Fix haskell comments in NERDCommenter
      let g:NERDCustomDelimiters = {
          \ 'haskell': {'left': '-- ', 'leftAlt': '{-', 'rightAlt': '-}'},
          \ 'cpp': {'left': '// ', 'leftAlt': '/*', 'rightAlt': '*/'},
          \ 'cmake': {'left': '# ', 'leftAlt': '#[[', 'rightAlt': ']]'},
          \ 'typescript': {'left': '// ', 'leftAlt': '/*', 'rightAlt': '*/'},
          \ 'pvl': {'left': '// ', 'leftAlt': '/* ', 'rightAlt': ' */'},
          \ 'tex': {'left': '% ', 'leftAlt': '\begin{comment}', 'rightAlt': '\end{comment}'}
          \ }

      " Map C-/ to toggle comments. Vim doesnt understand that but maps it to C-_
      " for some reason
      nmap <C-_>   <Plug>NERDCommenterToggle
      vmap <C-_>   <Plug>NERDCommenterToggle<CR>gv

      map <Leader><k6> <C-w>v<C-M-k5>
      map <Leader><k4> :q<CR><C-M-k4>

      " Sets search path for :find to pwd and every subfolder
      set path=**

      " Buffer cycling
      nnoremap <C-n> :bnext<CR>
      nnoremap <C-b> :bprevious<CR>

      " Move between buffers
      nmap <Tab> :bnext<CR>

      " Disable keyboard arrows
      nnoremap <up> <nop>
      nnoremap <left> <nop>
      nnoremap <right> <nop>
      nnoremap <down> <nop>

      " Also in insert mode!
      inoremap <up> <nop>
      inoremap <left> <nop>
      inoremap <right> <nop>
      inoremap <down> <nop>

      " Keymap for switching panels
      " map <silent> <SPACE>k :wincmd k<CR>
      " map <silent> <SPACE>j :wincmd j<CR>
      " map <silent> <SPACE>h :wincmd h<CR>
      " map <silent> <SPACE>l :wincmd l<CR>

      " keymap for resizing split windows
      map <left> :5winc ><CR>
      map <right> :5winc <<CR>
      map <down> :5winc +<CR>
      map <up> :5winc -<CR>

      " Selection Shortcuts
      nnoremap <Leader>w viw
      nnoremap <Leader>x vi(
      nnoremap <expr> <Leader>c 'vi' . (getline('.') =~ '"' ? '"' : "'")

      " Closes the preview window
      nnoremap <Leader><BS> :pc<CR>

      highlight Pmenu ctermfg=15 ctermbg=0 guifg=#ffffff guibg=#000000

      autocmd BufRead,BufNewFile *.launch setfiletype roslaunch

      function TrySwitchHeaderImpl()
          let file_name = expand('%:t:r')
          let extension = expand('%:t:e')
          if extension == "h"
              let target = ':find ' . file_name . '.cpp'
              execute target
          elseif extension == "hpp"
              let target = ':find ' . file_name . '.cpp'
              execute target
          elseif extension == "cc"
              let target = ':find ' . file_name . '.h'
              execute target
          else
              let target = ':find ' . file_name . '.h'
              execute target
          endif
      endfunction

      nnoremap <Leader>o :call TrySwitchHeaderImpl()<CR>

      " Jump to defintiion
      nnoremap <Leader>jd :YcmCompleter GoTo<CR>
      nnoremap <Leader>ff :YcmCompleter FixIt<CR>
      nnoremap <Leader>dd :YcmDiags<CR>
      nnoremap <Leader><Del> :lclose<CR>

      " behavior3 project settings
      au BufRead,BufNewFile,BufEnter ~/rtt/behavior3editor/* setlocal ts=2 sts=2 sw=2 expandtab

      " nice tabbing
      nnoremap <A-Left> :tabprevious<CR>
      nnoremap <A-Right> :tabnext<CR>
      nnoremap <C-t> :tabnew<CR>
      nnoremap <C-BS> :tabclose<CR>
      nnoremap <silent> <S-A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
      nnoremap <silent> <S-A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

      " nice view switching
      " nnoremap <C-h> <C-w>h
      " nnoremap <C-j> <C-w>j
      " nnoremap <C-k> <C-w>k
      " nnoremap <C-l> <C-w>l

      " Makes macro's faster by not rewdrawing inbetween
      set lazyredraw

      " Exit normal mode by typing jj
      inoremap df <ESC>

      " Makes sure j moves up one _visual_ line, and not a _semantic_ line
      nnoremap j gj
      nnoremap k gk

      " Triple space + w for word count
      nnoremap <Leader><Leader><Leader>w :w ! wc<CR>

      " Enters Goyo with nice line wrapping
      function Boyo()
              set wrap
              set linebreak
              set nolist 
              Goyo
      endfunction

      command Boyo call Boyo()

      " Makes sure part of the last line is also displayed
      set display+=lastline

      " Make sure nasm filetype is set
      autocmd BufNewFile,BufRead *.nasm 	set ft=nasm

      " Allow switching buffers without saving
      " When exiting if there are outstanding changes vim will error
      set hidden

      " Make sure ledger file format is set to something sane
      autocmd BufNewFile,BufRead *.ledger 	call SetLedgerOptions()
      function SetLedgerOptions()
              set expandtab
              set shiftwidth=4
              set softtabstop=4
              set tabstop=4
      endfunction

      set wildignore+=**/brain_build/**

      "let current_hostname = hostname()
      "if current_hostname == "minux"
              "let g:codestats_api_key = 'SFMyNTY.WW05aWFYTnRhV3B1Ym1GaGJRPT0jI01UUTVPUT09.x7gjjwVWi_UdtSezWcfKz5m9szp-10XSrAYxv55rfEQ'
      "elseif current_hostname == "bobe-ThinkPad-P50"
              "let g:codestats_api_key = 'SFMyNTY.WW05aWFYTnRhV3B1Ym1GaGJRPT0jI01UUTROZz09.DEtcXdJlwSoORyilfSHqsQYMR9Tti-KuemgB2W0NWH8'
      "else
              "echom "Current hostname not in codestats api key list!"
      "endif

      if !exists("g:ycm_semantic_triggers")
        let g:ycm_semantic_triggers = {}
      endif
      let g:ycm_semantic_triggers['typescript'] = ['.']

      let g:syntastic_typescript_checkers = ['tslint', 'tsc']

      "TURN OFF YOUCOMPLETEME
      " let g:loaded_youcompleteme = 1

      " Force python 3.6
      " let g:ycm_python_binary_path = 'python3.6'
      " Force python 3.6 for syntastic as well
      " let g:syntastic_python_checkers=[]
      " let g:syntastic_python_python_exe = 'python3.6'

      " Enable ln(ext) lp(revious) for errors
      let g:ycm_always_populate_locaction_list = 1
      let g:syntastic_always_populate_loc_list=1
      let g:syntastic_warning_symbol='⚠'
      let g:syntastic_warning_symbol='❌'
      let g:syntastic_check_on_open = 1

      " Make C-i print a date in the current document
      function InsertDate()
          r! date --rfc-2822
      endfunction
      function PrepareNewEntry()
          call InsertDate()
          execute "normal o- "
          startinsert!
      endfunction
      function GoToNewEntry()
          execute "normal Go"
          call PrepareNewEntry()
      endfunction
      nnoremap <Leader><C-i> :call InsertDate()<CR>

      set colorcolumn=80

      function SortTSImports()
          execute "normal! vip:sort /\"/\<CR>"
      endfunction

      command SortTSImports call SortTSImports()

      function SortTSImportElems()
          execute "normal! ^f{vi{d"
          let wordsWithSpace = split(getreg('"'), ",")
          let wordsWithoutSpace = []
          for wordWithSpace in wordsWithSpace
              call add(wordsWithoutSpace, substitute(wordWithSpace, " ", "", "g"))
          endfor
          execute "normal i " . join(sort(wordsWithoutSpace), ", ") . "\<Esc>"
      endfunction

      command SortTSImportElems call SortTSImportElems()

      set diffopt+=vertical

      " init-tags is located at:
      " /home/bobe/.local/bin/init-tags
      " augroup tags
      " au BufWritePost *.hs            silent !init-tags %
      " au BufWritePost *.hsc           silent !init-tags %
      " augroup END

      function SwitchToUnity3DAndPlay()
          call system("xdotool search --name \"Unity - Unity\" windowactivate --sync %1 && sleep 0.25s && xdotool key ctrl+p")
      endfunction

      nnoremap <F5> :call SwitchToUnity3DAndPlay()<CR>

      nnoremap <Leader>ccp :ClearAllCtrlPCaches<CR>

      " For moving the lines up and down with Alt+j
      nnoremap <A-j> :m .+1<CR>==
      nnoremap <A-k> :m .-2<CR>==
      inoremap <A-j> <Esc>:m .+1<CR>==gi
      inoremap <A-k> <Esc>:m .-2<CR>==gi
      vnoremap <A-j> :m '>+1<CR>gv=gv
      vnoremap <A-k> :m '<-2<CR>gv=gv

      autocmd BufNewFile,BufRead *.pvl set filetype=pvl
      autocmd BufNewFile,BufRead *.pvl set syntax=java

      " Interop with tmux - not sure if this breaks things without tmux
      set ttymouse=xterm2
      set mouse=a

      " Open file in gedit
      nnoremap <Leader>g :! gedit %<CR>


      " Wiki settings
      let g:wiki_root = '~/UNSAFE/wiki'
      " "zo" and "zc" can open and close folds

      " Formats whole file to what is set in textwidth
      nmap <Leader>F gggqG
      set omnifunc=syntaxcomplete#Complete
      " I want my "related" sections to be always visible.
      set nofoldenable
      imap <c-x><c-f> <plug>(fzf-complete-path)

      nnoremap <Leader>r :source $MYVIMRC<CR>
      autocmd FileType vim setlocal tabstop=4

      """" Custom zettelkasten bindings """"

      function! NewZettel()
         if exists("*strftime")
              let l:filename = strftime("%y%m%d%H%M.md")
              execute "edit ".l:filename
          else
              echohl ErrorMsg
              echomsg "Could not find strftime"
              echohl None
          endif
      endfunction

      function! OpenZettelkasten()
              cd ~/UNSAFE/Works/wiki
              execute "edit index.md"

          set textwidth=80

          " Add new bindings
          nnoremap <Leader>wz :call NewZettel()<CR>
          nnoremap <Leader>wn /[0-9]\{10\}\.md<CR>
      endfunction

      nnoremap <Leader>ww :call OpenZettelkasten()<CR>

      set tabstop=4
      set shiftwidth=4
      set expandtab
      set softtabstop=4
    '';
  };

  home.file.".ideavimrc".text = ''
    " Exit normal mode by typing jk
    inoremap df <ESC>

    nnoremap <C-h> <C-w>h
    nnoremap <C-j> <C-w>j
    nnoremap <C-k> <C-w>k
    nnoremap <C-l> <C-w>l
  '';
}
